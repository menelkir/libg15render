LIBG15RENDER
============

This is a library to render text and shapes into a buffer usable by the.
Logitech G15 Gaming Keyboard.
This library probably isn't very useful without libg15 and/or g15daemon.

Font support is now provided by FreeType2.  If you want font support, provide.
the --enable-ttf option to configure.  For font support in applications, you
must #define TTF_SUPPORT 1 before including libg15render.h and add
`freetype-config --cflags` to your $CFLAGS.

=======
Warning
=======
I'm discontinuing this after someone made a fuzz about a feature he decided he want a decade later. 
And as far I am concerned, Arch Linux's AUR administrators find this behavior just fine, so I'm not wasting my efforts on this anymore.
I can still fix issues as I always did and help via mail, but keep in mind Arch Linux is impossible to be supported.

============
Requirements
============

- libg15
